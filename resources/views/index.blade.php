<?php
URL::forceRootUrl(Config::get('app.url'));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('vendor/build/css/app.css') }}">
    <title>Admin panel</title>
</head>

<body>
<div id="app">

</div>
</body>
<script>
window.env = {
    appDashboardUrl: '{{ url(config('dashboard.url_prefix')) }}/',
    appUrl: '{{ url('/') }}'
};
</script>
<script src="{{ asset('vendor/build/js/app.js') }}"></script>
</html>
