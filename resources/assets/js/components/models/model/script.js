/**
 * Created by alex on 17.07.17.
 */
import Table from '../../tables/table_clasic/TableClasic.vue';
import ModalNewModel from '../../modals/add_new_model/ModalAddNewModel.vue';
import ModalCard from '../../modals/modal_card/ModalCard.vue';

export default {
    props: {
        showStandardTopButtons: {
            type: Boolean,
            default: true
        },
        refreshItem: Array
    },
    components: {
        'modal-card': ModalCard,
        'clasic-table': Table,
        'modal-new-modal': ModalNewModel
    },
    data: function () {
        return {
            model: '',
            modelData: [],
            modelActions: [],
            columns: [],
            modelStructure: [],
            canDelete: false,
            canUpdate: false,
            canEdit: false,
            canShow: false,
            canStore: false,
            canCreate: false,
            showModalNewModel: false,
            updateModelId: 0,
        }
    },
    created() {
        this.model = this.$route.params.model;
        if (this.model === undefined) {
            this.model = this.$route.name;
        }

        this.$axios.getItems(this.model).then(response => {
            this.modelData = response.data.data;
            this.modelActions = response.data.actions;
            this.modelStructure = this.getNewModelStructure(response.data.columns[0]);
            this.columns = Object.keys(response.data.data[0]);
        }).catch(response => {
            console.error(response);
        });
    },
    watch: {
        '$route'(to, from) {
            this.model = to.params.model;
            this.$axios.getItems(this.model).then(response => {
                this.modelData = response.data.data;
                this.modelActions = response.data.actions;
                this.modelStructure = this.getNewModelStructure(response.data.columns[0]);
                this.columns = Object.keys(response.data.data[0]);
            }).catch(response => {
                console.error(response);
            });
        },
    },
    computed: {

    },
    updated() {
        this.setModelPolicies();
    },
    methods: {
        getNewModelStructure(data) {
            let newModelStructure = [];
            for (let i = 0; data.columns.length > i; i++) {
                let input = {};
                input.column = data.columns[i].name;
                input.inputType = data.columns[i].input.type;
                input.inputIcon = data.columns[i].input.icon;
                let rules = [];
                for (let j = 0; data.columns[i].input.rules.length > j; j++) {
                    rules.push(data.columns[i].input.rules[j].rule);
                }
                input.rules = rules.join('|');
                input.value = null;
                newModelStructure.push(input);
            }
            this.$emit('modelStructure', newModelStructure);
            return newModelStructure;
        },
        insertNewRow(item) {
            this.modelData.push(item);
        },
        deleteRow(modelItem) {
            this.$axios.deleteItem(this.model, modelItem.id).then(
                response => {
                    if (response.data.status) {
                        this.modelData = this.modelData.filter(function (modelItem) {
                            return modelItem.id != response.data.id;
                        }, response);
                    }
                }).catch(response => {
            });
        },
        updateRow(modelItem) {
            this.showModalNewModel = true;

            if (!(Object.keys(modelItem).length === 0 && modelItem.constructor === Object)) {
                for (let i = 0; this.modelStructure.length > i; i++) {
                    this.modelStructure[i].value = modelItem[this.modelStructure[i].column];
                }
            }

            this.updateModelId = modelItem.id;
        },
        refreshRow(updatedModelItem) {
            this.updateModelId = 0;
            for (let i = 0; this.modelStructure.length > i; i++) {
                this.modelStructure[i].value = null;
            }
            this.modelData = this.modelData.map(function (modelItem) {
                if (modelItem.id === updatedModelItem.id) {
                    modelItem = updatedModelItem;
                    return modelItem;
                }
                return modelItem;
            }, updatedModelItem);
        },
        modelCan(action) {
            let currentAction = this.model + '.' + action;
            if (this.modelActions.includes(currentAction)) {
                this.$emit(this.model + '_' + action, true);
                return true;
            }
            return false;
        },
        setModelPolicies() {
            this.canDelete = this.modelCan('destroy');
            this.canUpdate = this.modelCan('update');
            this.canEdit = this.modelCan('edit');
            this.canShow = this.modelCan('show');
            this.canStore = this.modelCan('store');
            this.canCreate = this.modelCan('create');
        }
    }
}