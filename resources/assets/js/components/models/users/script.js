/**
 * Created by alex on 19.07.17.
 */
import ModalView from '../model/ModelView.vue';
import ModalCard from '../../modals/modal_card/ModalCard.vue';
export default {
    components: {
        'model-view': ModalView,
        'modal-card': ModalCard
    },
    data: function () {
        return {
            canStore: false,
            showModalNewModel: false,
            systemRoles: [],
            modelStructure: [],
            selectedRoles: [],
            buttonSaveStatus: false
        }
    },
    computed: {
        changeButtonStatus: function () {
            if (this.$validator.errorBag.errors.length === 0) {
                for (let i = 0; this.modelStructure.length > i; i++) {
                    if (this.modelStructure[i].value === null) {
                        return this.buttonSaveStatus = false;
                    }
                }
                return this.buttonSaveStatus = true;
            } else {
                this.buttonSaveStatus = false;
            }
            return this.buttonSaveStatus;
        },
    },
    methods: {
        setPoliciStore(can){
            this.canStore = can;
        },
        showModal(){
            this.showModalNewModel = true;
            this.getDataForCreate();
        },
        getDataForCreate(){
            this.$axios.getItems('users/create').then(response => {
               this.systemRoles = response.data.roles;
            }).catch(response => {
                console.error(response);
            });
        },
        setModelStructure(data){
            this.modelStructure = data;
        },
        isCorrectValue(value, inputName)
        {
            if (!this.errors.has(inputName) && value != null) {
                return true;
            }

            return false
        },
        storeNewUser()
        {
            if (!this.buttonSaveStatus) {
                return;
            }

            this.$axios.postItem('users',
                {
                    user: this.prepareDataForPost(),
                    roles: this.selectedRoles
                }
                ).then(response => {
                this.buttonSaveStatus = false;
                for (let i = 0; this.modelStructure.length > i; i++) {
                    this.modelStructure[i].value = null;
                }
                this.showModalNewModel = false;
                this.$router.go(this.$router.currentRoute);
            }).catch(response => {
                console.error(response);
            });
        },
        prepareDataForPost(){
            let data = {};
            for (let i = 0; this.modelStructure.length > i; i++) {
                data[this.modelStructure[i].column] = this.modelStructure[i].value;
            }
            return data;
        },
    }
}