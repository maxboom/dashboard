/**
 * Created by alex on 10.07.17.
 */
import {appUrl} from '../../../env';
import ModalCard from '../../modals/modal_card/ModalCard.vue';
import ModalClassic from '../../modals/classic/ModalClassic.vue';
import Table from '../../tables/table_clasic/TableClasic.vue';

export default {
    components: {
        'modal-card': ModalCard,
        'modal-classic': ModalClassic,
        'table-clasic': Table
    },
    data() {
        return {
            oauthClients: [],
            showModalCard: false,
            showModalClassicSecret: false,
            buttonSaveStatus: false,
            hasErrors: false,
            newClient: {
                'name': '',
                'redirect': ''
            },
            currentSecret: ''
        }
    },
    created() {
        this.$axios.get(appUrl + 'oauth/clients').then(response => {
            this.oauthClients = response.data;
            console.log(response);
        }).catch(response => {
            console.error(response);
        });
    },
    computed: {
        changeButtonStatus: function () {
            if (this.$validator.errorBag.errors.length === 0) {
                this.buttonSaveStatus = true;
            } else {
                this.buttonSaveStatus = false;
            }
            return this.buttonSaveStatus;
        }
    },
    methods: {
        addNewClient()
        {
            if (!this.buttonSaveStatus) {
                return;
            }

            this.$axios.post(appUrl + 'oauth/clients', {
                name: this.newClient.name,
                redirect: this.newClient.redirect
            }).then(response => {
                this.oauthClients.push(response.data);
                console.log(response);
                this.newClient.name = '';
                this.newClient.redirect = '';
                this.buttonSaveStatus = false;
                this.showModalCard = false;
            }).catch(response => {
                console.error(response);
            });
        },
        showSecret(secret){
            this.currentSecret = secret;
            this.showModalClassicSecret = true;
        },
        clientDelete(id){
            this.$axios.delete(appUrl + 'oauth/clients/' + id).then(response => {
                this.oauthClients = this.oauthClients.filter(function (item) {
                    if (item.id !== id) {
                        return item;
                    }
                });
            }).catch(response => {
                console.error(response);
            });
        }
    }
}


