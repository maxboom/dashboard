/**
 * Created by alex on 05.07.17.
 */
/**
 * Created by alex on 20.06.17.
 */
import axios from 'axios';
import {getTokenUrl, clientId, clientSecret} from '../../env';

export default {
    data: function () {
        return {
            grant_type: 'password',
            client_id: clientId,
            client_secret: clientSecret,
            email: '',
            password: '',
            scope: '',
        }
    },
    methods: {
        handleLoginFormSubmit() {
            console.log(this.data);
            axios.post(getTokenUrl, {
                client_id: this.client_id,
                client_secret: this.client_secret,
                grant_type: this.grant_type,
                username: this.email,
                password: this.password,
                //scope: '*',
            }).then(response => {
                this.$auth.setToken(
                    response.data.access_token,
                    response.data.expires_in + Date.now()
                );
                console.log(response);
            }).catch(response => {
                console.error(response);
            });
        }
    }
}