/**
 * Created by alex on 06.07.17.
 */

export default {
    props: ['isAuth'],
    methods: {
        logout(){
            this.$auth.destroyAllTokenData();
            this.$router.go('login');
        }
    }
}