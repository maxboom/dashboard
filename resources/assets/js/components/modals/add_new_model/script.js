/**
 * Created by alex on 18.07.17.
 */
export default {
    props: {
        modelStructure: Array,
        model: String,
        updateModelId: Number
    },
    data () {
        return {
            buttonSaveStatus: false,
        }

    },
    computed: {
        changeButtonStatus: function () {
            if (this.$validator.errorBag.errors.length === 0) {
                for (let i = 0; this.modelStructure.length > i; i++) {
                    if (this.modelStructure[i].value === null) {
                        return this.buttonSaveStatus = false;
                    }
                }
                return this.buttonSaveStatus = true;
            } else {
                this.buttonSaveStatus = false;
            }
            return this.buttonSaveStatus;
        },
    },
    methods: {
        addNewRow()
        {
            if (!this.buttonSaveStatus) {
                return;
            }

            if (this.updateModelId !== 0) {
                this.updateRow();
                return;
            }

            this.$axios.postItem(this.model, this.prepareDataForPost()).then(response => {
                this.buttonSaveStatus = false;
                for (let i = 0; this.modelStructure.length > i; i++) {
                    this.modelStructure[i].value = null;
                }
                this.$emit('closeModal');
                this.$emit('itemSaved', response.data);
            }).catch(response => {
                console.error(response);
            });
        },
        updateRow(){
            this.$axios.putItem(
                this.model,
                this.updateModelId,
                this.prepareDataForPost()).then(response => {
                this.buttonSaveStatus = false;
                for (let i = 0; this.modelStructure.length > i; i++) {
                    this.modelStructure[i].value = null;
                }
                this.$emit('closeModal');
                this.$emit('itemUpdated', response.data);
            }).catch(response => {
                console.error(response);
            });
        },
        prepareDataForPost(){
            let data = {};
            for (let i = 0; this.modelStructure.length > i; i++) {
                data[this.modelStructure[i].column] = this.modelStructure[i].value;
            }
            return data;
        },
        isCorrectValue(value, inputName)
        {
            if (!this.errors.has(inputName) && value != null) {
                return true;
            }

            return false
        }
    }
}