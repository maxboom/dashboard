/**
 * Created by alex on 15.07.17.
 */
export default {
    props: {
        columns: Array,
        data: Array,
        actions: Array
    },
    data: function () {
     return {
         model: '',
         canDelete: false,
         canUpdate: false,
         canEdit: false,
         canShow: false,
         canStore: false,
         canCreate: false
     }
    },
    created(){
        this.model = this.$route.params.model;
        if (this.model === undefined) {
            this.model = this.$route.name;
        }
    },
    updated(){
        this.canDelete = this.modelCan('destroy');
        this.canUpdate = this.modelCan('update');
        this.canEdit = this.modelCan('edit');
        this.canShow = this.modelCan('show');
        this.canStore = this.modelCan('store');
        this.canCreate = this.modelCan('create');
    },
    watch: {
        '$route' (to, from) {
            this.model = to.params.model;
        }
    },
    methods: {
        upperFirst(item){
            return item[0].toUpperCase() + item.substring(1);
        },
        modelCan(action){
           if (this.actions.includes(this.model + '.' + action)) {
                return true;
           }
           return false;
        }
    }
}