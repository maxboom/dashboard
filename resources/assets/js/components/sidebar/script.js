/**
 * Created by alex on 06.07.17.
 */
import Menu from './menu/Menu.vue';

export default {
    components: {
      'menu-item': Menu
    },
    data() {
        return {
            menus: []
        }
    },
    methods: {
        getMenuChildren(menu, parent) {
            var children = menu.filter(item => {
                return item['parent_id'] == parent;
            });

            // console.log(menu, parent, children);
            // console.log(children);

            return children.map(item => {
                item['children'] = this.getMenuChildren(menu, item['id'])
                return item;
            });



            // menu['children'] = menu.map(menuItem => {
            //     var menus = menu.filter(function (subMenuItem) {
            //         return menu['id'] == subMenuItem['parent_id'];
            //     })

            //     console.log(menus);

            //     // if (menu['id'] == menuItem['parent_id']) {
            //     //menuItem['children'] = this.getMenuChildren(menus);
            //     // };

            //     return menuItem;
            // });
        }
    },
    mounted() {
        this.$axios.get('api/dashboard_menus', {}).then(response => {
            // console.log(this.getMenuChildren(response.data.data, null));
            this.menus = Object.values(this.getMenuChildren(response.data.data, null));
        }).catch(response => {
            console.error(response);
        });
    }
}