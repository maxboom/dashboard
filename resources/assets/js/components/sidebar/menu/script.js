/**
 * Created by alex on 18.07.17.
 */
import Menu from './Menu.vue';

export default {
    name: 'menu-item',
    components: {
      'menu-item': Menu
    },
    props: {
        model: Array,
        open: false
    },
    data() {
        return {
            childOpen: false,
        }
    },
    methods: {
        isFolder: function (menuItem) {
            return menuItem.children &&
                menuItem.children.length
        },
        toggle: function (menuItem) {
            if (this.isFolder(menuItem)) {
                this.childOpen = !this.childOpen;
            }
        },
    }
}