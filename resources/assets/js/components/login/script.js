/**
 * Created by alex on 20.06.17.
 */

import {appDashboardUrl} from '../../env';

export default {
    data: function () {
        return {
            email: '',
            password: '',
            scope: '*',
        }
    },
    methods: {
        handleLoginFormSubmit() {
            this.$axios.post(appDashboardUrl + 'api/login', {
                email: this.email,
                password: this.password,
            }).then(response => {
                console.log(response);
                this.$auth.setToken(
                    response.data.access_token,
                    response.data.expires_in,
                    response.data.refresh_token,
                );
                this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.$auth.getToken();
                this.$router.push("/");
            }).catch(response => {
                console.error(response);
            });
        }
    }
}