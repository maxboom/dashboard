import Vue from 'vue';
import VeeValidate from 'vee-validate';
import App from './App.vue';
import Router from './routes';

const validatorConfig = {
    errorBagName: 'errors', // change if property conflicts.
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    enableAutoClasses: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'input|blur',
    inject: true
};
Vue.use(VeeValidate, validatorConfig);
Vue.filter('capitalize', function (string) {
    return string[0].toUpperCase() + string.slice(1);
});
new Vue({
    el: '#app',
    render: h => h(App),
    router: Router
});