import axios from 'axios';
import {appDashboardUrl} from '../../env';

export default function (Vue) {
    Vue.axios = axios.create({
        baseURL: appDashboardUrl,
        timeout: 1000,
        headers: {
           // 'X-Custom-Header': 'foobar',
        },
    });
    Vue.axios.getItems = (model, data) => {
        return Vue.axios.get('api/' +model);
    };
    Vue.axios.deleteItem = (model, id) => {
        return Vue.axios.delete('api/' +model+ '/' + id);
    };
    Vue.axios.postItem = (model, data) => {
        return Vue.axios.post('api/' +model, data);
    };
    Vue.axios.putItem = (model, modelId, data) => {
        return Vue.axios.put('api/' + model + '/' + modelId, data);
    };
    Object.defineProperties(Vue.prototype, {
        $axios: {
            get: () => {
                return Vue.axios;
            }
        }
    });
}