/**
 * Created by alex on 05.07.17.
 */
import {appDashboardUrl} from '../../env';

export default function (Vue) {
    Vue.auth = {
        setToken(token, expiration, refreshToken) {
            localStorage.setItem('token', token);
            localStorage.setItem('refreshToken', refreshToken);
            localStorage.setItem('expiration', expiration + Date.now());
            localStorage.setItem('expires_in', expiration);
        },
        getToken() {
            var token = localStorage.getItem('token');
            var expiration = localStorage.getItem('expiration');

            if (!token || !expiration) {
                return null;
            }

            if (Date.now() > parseInt(expiration)) {
                this.destroyToken();
                return null;
            } else {
                return token;
            }
        },
        destroyToken() {
            localStorage.removeItem('token');
            localStorage.removeItem('expires_in');
        },
        destroyAllTokenData() {
            this.destroyToken();
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('expiration');
        },
        isAuthenticated() {
            if (this.getToken() && this.getToken() != 'undefined') {
                return true;
            } else {
                return false;
            }
        }
    };
    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth
            }
        }
    });
}