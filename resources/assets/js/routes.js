/**
 * Created by alex on 05.07.17.
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import Auth from './packages/auth/Auth';
import Axios from './packages/axios/Axios';


import Dashboard from './components/dashboard/Dashboard.vue';
import Login from './components/login/Login.vue';
import Register from './components/register/Register.vue';

//Models
import ModelView from './components/models/model/ModelView.vue';
import Users from './components/models/users/Users.vue';
import OauthClients from './components/models/oauth/OauthClients.vue';


import {appDashboardUrl} from './env';

Vue.use(VueRouter);
Vue.use(Auth);
Vue.use(Axios);

Vue.axios.defaults.headers.common = {
    'Authorization': 'Bearer ' + Vue.auth.getToken(),
    'X-Requested-With': 'XMLHttpRequest',
};

const routes = [
    {
        path: '/',
        component: Dashboard,
        meta: {
            forAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            forVisitors: true
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            forVisitors: true
        }
    },
    //Oauth
    {
        path: '/oauth_clients',
        component: OauthClients,
        name: 'oauthClients',
        meta: {
            forAuth: true
        }
    },
    {
        path: '/users',
        component: Users,
        name: 'users',
        meta: {
            forAuth: true
        }
    },
    {
        path: '/:model',
        component: ModelView,
        meta: {
            forAuth: true
        }
    }
];

const router = new VueRouter({
    // mode: 'history',
    routes: routes
});


router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuthenticated()) {
                next({
                    path: '/'
                });
            } else {
                next();
            }
        } else if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuthenticated()) {
                var refreshToken = localStorage.getItem('refreshToken');
                Vue.axios.post( appDashboardUrl + 'api/refresh-token', {
                    refresh_token: refreshToken,
                    scope: '',
                }).then(response => {
                    Vue.auth.setToken(
                        response.data.access_token,
                        response.data.expires_in,
                        response.data.refresh_token,
                    );
                    Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();
                    next();
                }).catch(response => {
                    Vue.auth.destroyAllTokenData();
                    next({
                        path: '/login'
                    });
                });
            } else {
                next();
            }
        } else {
            next();
        }
    }
);

export default router;