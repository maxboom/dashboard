<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userModelClass = config('dashboard.user_model_class');

        $user = $userModelClass::create([
            'login' => 'admin',
            'name' => 'admin',
            'email' => 'admin@dashboard.io',
            'password' => 'admin'
        ]);

        $this->_createToken($user->id);

        $role = \MaxBoom\Dashboard\Models\UserRole::create([
            'ident' => 'admin',
            'caption' => 'Admins'
        ]);

        $role->users()->attach($user);

        $area = \MaxBoom\Dashboard\Models\Area::create([
            'caption' => 'Dashboard'
        ]);

        $action = $area->actions()->create([
            'action' => 'dashboard',
            //'callback' => 'function() { die("it`s work!"); }'
        ]);

        $role->actions()->attach($action);


        $this->_seedArea(
            $role,
            \App\User::class,
            'Users'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\Menu::class,
            'Menus'
            );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\Area::class,
            'Areas'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\AreaAction::class,
            'Areas actions'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\AreaColumn::class,
            'Areas columns'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\Input::class,
            'Inputs'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\InputRule::class,
            'Inputs rules'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\OauthClient::class,
            'Oauth clients'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\OauthAccessToken::class,
            'Oauth access tokens'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\UserRole::class,
            'Roles'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\UserRole2User::class,
            'Users roles'
        );

        $this->_seedArea(
            $role,
            \MaxBoom\Dashboard\Models\UserRole2Menu::class,
            'Roles menu'
        );
    }

    private function _createToken($userId)
    {
        $client = (new \MaxBoom\Dashboard\Models\OauthClient)->forceFill([
            'user_id' => $userId,
            'name' => 'super_admin_api',
            'secret' => str_random(40),
            'redirect' => '',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => false,
            'url' => config('app.url')
        ]);

        $client->save();
    }

    private function _seedArea($role, $model, $caption)
    {
        $table = (new $model)->getTable();

        $userAreaMenu = \MaxBoom\Dashboard\Models\Area::create([
            'table' => $table,
            'model' => $model,
            'caption' => $caption
        ]);

        $indexAtion = $userAreaMenu->actions()->create([
            'name' => $table.'.index',
        ]);

        $role->actions()->attach($indexAtion);

        $createAction = $userAreaMenu->actions()->create([
            'name' => $table.'.create',
        ]);

        $role->actions()->attach($createAction);

        $storeAction = $userAreaMenu->actions()->create([
            'name' => $table.'.store',
        ]);

        $role->actions()->attach($storeAction);

        $showAction = $userAreaMenu->actions()->create([
            'name' => $table.'.show',
        ]);

        $role->actions()->attach($showAction);

        $editAction = $userAreaMenu->actions()->create([
            'name' => $table.'.edit',
        ]);

        $role->actions()->attach($editAction);

        $updateAction = $userAreaMenu->actions()->create([
            'name' => $table.'.update',
        ]);

        $role->actions()->attach($updateAction);

        $destroyAction = $userAreaMenu->actions()->create([
            'name' => $table.'.destroy',
        ]);

        $role->actions()->attach($destroyAction);
    }
}
