<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 16:01
 */

use Illuminate\Database\Seeder;

class InputsSeeder extends Seeder
{
    public function run()
    {
        $this->_seedInputs(
            [
                [
                    'name' => 'name',
                    'type' => 'text',
                    'rules' => [
                        'required',
                    ],
                ],
                [
                    'name' => 'email',
                    'type' => 'email',
                    'rules' => [
                        'required',
                        'email',
                    ],
                ],
                [
                    'name' => 'phone',
                    'type' => 'tel',
                    'rules' => [
                        'required',
                        'phone',
                    ],
                ],
                [
                    'name' => 'string',
                    'type' => 'text',
                    'rules' => [
                        'required',
                        'string',
                    ],
                ],
                [
                    'name' => 'password',
                    'type' => 'password',
                    'rules' => [
                        'required',
                        'min:8',
                    ],
                ],
                [
                    'name' => 'caption',
                    'type' => 'text',
                    'rules' => [
                        'required',
                    ],
                ],
                [
                    'name' => 'url',
                    'type' => 'url',
                    'rules' => [
                        'required',
                        'url',
                    ],
                ],
                [
                    'name' => 'integer',
                    'type' => 'text',
                    'rules' => [
                        'required',
                        'integer',
                    ],
                ],
            ]
        );
    }

    private function _seedInputs($inputs)
    {
        foreach ($inputs as $input) {
            $newInput = new \MaxBoom\Dashboard\Models\Input();
            $newInput->name = $input['name'];
            $newInput->type = $input['type'];
            $newInput->save();
            foreach ($input['rules'] as $ruleName) {
                $rule = \MaxBoom\Dashboard\Models\InputRule::whereRule(
                    $ruleName
                )->first();
                if ($rule) {
                    $newInput->rules()->attach($rule);
                }
            }
        }
    }
}