<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 17:24
 */
use Illuminate\Database\Seeder;

class AreaColumnsTableSeeder extends Seeder
{
    public function run()
    {
        $this->_seedColumns(
            'users',
            [
                [
                    'column' => 'name',
                    'input' => 'name',
                ],
                [
                    'column' => 'email',
                    'input' => 'email',
                ],
                [
                    'column' => 'password',
                    'input' => 'password',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'menus',
            [
                [
                    'column' => 'url',
                    'input' => 'string',
                ],
                [
                    'column' => 'icon_class',
                    'input' => 'string',
                ],
                [
                    'column' => 'parent_id',
                    'input' => 'integer',
                ],
                [
                    'column' => 'caption',
                    'input' => 'name',
                ],
            ]
        );
        $this->_seedColumns(
            config('dashboard.table_prefix').'areas',
            [
                [
                    'column' => 'table',
                    'input' => 'name',
                ],
                [
                    'column' => 'model',
                    'input' => 'name',
                ],
                [
                    'column' => 'caption',
                    'input' => 'caption',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'inputs',
            [
                [
                    'column' => 'name',
                    'input' => 'name',
                ],
                [
                    'column' => 'type',
                    'input' => 'string',
                ],
                [
                    'column' => 'icon',
                    'input' => 'string',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'input_rules',
            [
                [
                    'column' => 'rule',
                    'input' => 'string',
                ],
                [
                    'column' => 'description',
                    'input' => 'string',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'user_roles',
            [
                [
                    'column' => 'ident',
                    'input' => 'string',
                ],
                [
                    'column' => 'caption',
                    'input' => 'name',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'user_roles2users',
            [
                [
                    'column' => 'role_id',
                    'input' => 'integer',
                ],
                [
                    'column' => 'user_id',
                    'input' => 'integer',
                ],
            ]
        );

        $this->_seedColumns(
            config('dashboard.table_prefix').'user_roles2menus',
            [
                [
                    'column' => 'role_id',
                    'input' => 'integer',
                ],
                [
                    'column' => 'menu_id',
                    'input' => 'integer',
                ],
            ]
        );
    }

    private function _seedColumns($table, $columns)
    {
        $areas = \MaxBoom\Dashboard\Models\Area::whereTable($table)->get();
        foreach ($areas as $area) {
            foreach ($columns as $column) {
                $newRow = \MaxBoom\Dashboard\Models\AreaColumn::create(
                    [
                        'name' => $column['column'],
                        'area_id' => $area->id,
                        'input_id' => \MaxBoom\Dashboard\Models\Input::whereName(
                            $column['input']
                        )->first()->id,
                    ]
                );
            }
        }

    }
}