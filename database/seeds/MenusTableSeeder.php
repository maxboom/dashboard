<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 17:24
 */
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    public function run()
    {

        $this->_createMenu('/', 'Main');
        $optionsMenu = $this->_createMenu('users', 'Options');

        foreach (\MaxBoom\Dashboard\Models\Area::where(
            'table',
            '!=',
            null
        )->where('table', '!=', 'users')->get() as $area) {
            $this->_createMenu($area->table, $area->caption, $optionsMenu->id);
        }
    }

    private function _createMenu($url, $caption, $parent_id = null)
    {
        $menu = \MaxBoom\Dashboard\Models\Menu::create(
            [
                'url' => $url,
                'caption' => $caption,
                'parent_id' => $parent_id,
            ]
        );

        $this->_attachMenuToRole($menu);

        return $menu;
    }

    private function _attachMenuToRole($menu)
    {
        $role = \MaxBoom\Dashboard\Models\UserRole::whereIdent('admin')->first(
        );

        if ($role) {
            $menu->roles()->attach($role);
        }
    }
}