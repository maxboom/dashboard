<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 16:01
 */

use Illuminate\Database\Seeder;

class InputRulesSeeder extends Seeder
{
    public function run()
    {
        $this->_seedRules([
            [
                'rule' => 'array',
                'description' => 'The field under validation must be a PHP array.'
            ],
            [
                'rule' => 'boolean',
                'description' => 'The field under validation must be able to be cast as a boolean. Accepted input are true,  false, 1, 0, "1", and "0".'
            ],
            [
                'rule' => 'date',
                'description' => 'The field under validation must be a valid date according to the strtotime PHP function.'
            ],
            [
                'rule' => 'email',
                'description' => 'The field under validation must be formatted as an e-mail address.'
            ],
            [
                'rule' => 'file',
                'description' => 'The field under validation must be a successfully uploaded file.'
            ],
            [
                'rule' => 'image',
                'description' => 'The file under validation must be an image (jpeg, png, bmp, gif, or svg)'
            ],
            [
                'rule' => 'integer',
                'description' => 'The field under validation must be an integer.'
            ],
            [
                'rule' => 'ip',
                'description' => 'The field under validation must be an IP address.'
            ],
            [
                'rule' => 'json',
                'description' => 'The field under validation must be a valid JSON string.'
            ],
            [
                'rule' => 'required',
                'description' => 'The field under validation must be present in the input data and not empty.'
            ],
            [
                'rule' => 'string',
                'description' => 'The field under validation must be a string. If you would like to allow the field to also be null, you should assign the nullable rule to the field.'
            ],
            [
                'rule' => 'phone',
                'description' => 'The field must be phone'
            ],
            [
                'rule' => 'min:8',
                'description' => 'Min 8 characters'
            ],
            [
                'rule' => 'url',
                'description' => 'Must be url'
            ]
        ]);
    }

    private function _seedRules($rules)
    {
        foreach ($rules as $rule){
            \MaxBoom\Dashboard\Models\InputRule::create(
                [
                    'rule' => $rule['rule'],
                    'description' => $rule['description']
                ]
            );
        }
    }
}