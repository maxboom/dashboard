<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('dashboard.table_prefix') . 'inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->default('text');
            $table->string('icon')->default('fa-keyboard-o');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('dashboard.table_prefix') . 'inputs');
    }
}
