<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('dashboard.table_prefix') . 'area_columns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('area_id')->unsigned();
            $table->integer('input_id')->unsigned();
            $table->timestamps();

            $table->foreign('input_id')
                ->references('id')->on(config('dashboard.table_prefix') . 'inputs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('area_id')
                ->references('id')->on(config('dashboard.table_prefix') . 'areas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('dashboard.table_prefix') . 'area_columns');
    }
}
