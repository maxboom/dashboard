<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInput2rulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('dashboard.table_prefix') . 'input2rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('input_id')->unsigned();
            $table->integer('rule_id')->unsigned();
            $table->timestamps();
            $table->foreign('input_id')
                ->references('id')->on(config('dashboard.table_prefix') . 'inputs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('rule_id')
                ->references('id')->on(config('dashboard.table_prefix') . 'input_rules')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('dashboard.table_prefix') . 'input2rules');
    }
}
