<?php


return [
    'url_prefix' => 'dashboard',
    'table_prefix' => 'dashboard_',
    'user_model_class' => \MaxBoom\Dashboard\Models\DashboardUser::class,
    'pagination' => 15,
    'app_client_id' => env('APP_CLIENT_ID', 1)
];