<?php

namespace MaxBoom\Dashboard\Http\Middleware;

use Closure;
use MaxBoom\Dashboard\Models\OauthClient;
use Route;

class RequestUrlAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()) {
            $userClients = OauthClient::whereUserId(
                $request->user()->id
            )->whereRevoked(false)->where('url', 'like', '%'.$request->getHost().'%')->get();

            if ($userClients->isEmpty()) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        }

        return $next($request);
    }
}
