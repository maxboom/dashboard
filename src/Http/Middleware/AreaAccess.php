<?php

namespace MaxBoom\Dashboard\Http\Middleware;

use App\User;
use Closure;
use MaxBoom\Dashboard\Models\UserRole2User;
use MaxBoom\Dashboard\Models\UserRole;
use MaxBoom\Dashboard\Models\AreaAction;
use Route;

class AreaAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = AreaAction::whereAction(
            Route::getCurrentRoute()->uri()
        )->orWhere(
            'name',
            Route::currentRouteName()
        )->whereNotNull('name')->first();

        if (!$action || $action->roles->isEmpty()) {
            return $next($request);
        }

        if (!$request->user()) {
            return redirect('/dashboard/login');
        }

        $access = $request->user()->can($action, $action);

        if (!$access) {
            return abort(401);
        }

        return $next($request);
    }
}
