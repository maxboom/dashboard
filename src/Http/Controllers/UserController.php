<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.07.17
 * Time: 18:31
 */
namespace MaxBoom\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaxBoom\Dashboard\Models\DashboardUser;
use MaxBoom\Dashboard\Models\UserRole;
use Route;
use DB;
use Validator;

class UserController extends Controller
{
    use ControllerTrait;

    public function create()
    {
        $action = $this->getCurrentAction();
        return [
            'roles' => UserRole::all(),
        ];
    }

    public function store(Request $request)
    {
        $action = $this->getCurrentAction();

        if (!$action) {
            return response()->json(['error' => 'Undefined action'], 404);
        }

        $validate = $this->_validate($action, $request);

        if ($validate !== true) {
            return response()->json($validate, 404);
        }

        $requestData = $request->all();

        $userRoles = UserRole::whereIn('ident', $requestData['roles'])->get();

        if ($action->area->model) {
            $newModel = DashboardUser::create($requestData['user']);
            $newModel->roles()->attach($userRoles);
            $this->_createToken($newModel->id);
            return $newModel;
        }

        return response()->json(['error' => 'Model not saved'], 404);
    }

    private function _createToken($userId)
    {
        $client = (new \MaxBoom\Dashboard\Models\OauthClient)->forceFill([
            'user_id' => $userId,
            'name' => 'panel_api',
            'secret' => str_random(40),
            'redirect' => '',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => false,
            'url' => config('dashboard.app_url')
        ]);

        return $client->save();
    }

    private function _validate($action, $request)
    {
        $area = $action->area()->with(
            ['columns.input.rules']
        )->first();

        if (!$area) {
            return [
                'errors' => 'model not set!!',
            ];
        }

        $rules = $area->columns->map(
            function ($item) {
                $columnRules = $item->input->rules->map(
                    function ($rule) {
                        return $rule->rule;
                    }
                )->toArray();

                return [
                    $item->name => implode('|', $columnRules),
                ];

            }
        );

        $requestData = $request->all();

        $validator = Validator::make(
            $requestData['user'],
            $rules->collapse()->toArray()
        );

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
            ];
        }

        return true;
    }
}