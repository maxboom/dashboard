<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12.07.17
 * Time: 17:18
 */

namespace MaxBoom\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use MaxBoom\Dashboard\Models\OauthClient;
use anlutro\cURL\cURL;

class AuthController extends Controller
{
    private $http;
    private $clients;

    public function __construct()
    {
        $this->http = new cURL();
    }

    public function login()
    {
        if (!Auth::attempt(
            ['email' => request('email'), 'password' => request('password')]
        )
        ) {
            return $this->_createResponseUnauthorise();
        }

        $client = app('DashboardOauthClient');

        if (!$client) {
            return $this->_createResponseUnauthorise();
        }

        $response = $this->http->post(url('oauth/token'), [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => request('email'),
                'password' => request('password'),
                'scope' => '',
            ]
        );

        return response()->json(
            json_decode($response->body, true),
            200
        );
    }

    public function refreshToken()
    {
        // $client = $this->_getOauthClient();
        $client = app('DashboardOauthClient');

        if (!$client) {
            return $this->_createResponseUnauthorise();
        }

        $response = $this->http->post(url('oauth/token'), [
                'grant_type' => 'refresh_token',
                'refresh_token' => request('refresh_token'),
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'scope' => '',
            ]
        );

        return response()->json(
            json_decode($response->body, true),
            200
        );
    }

    private function _createResponseUnauthorise()
    {
        return response()->json(['error' => 'Unauthorized'], 401);
    }
}