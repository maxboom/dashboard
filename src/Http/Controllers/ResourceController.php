<?php

namespace MaxBoom\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaxBoom\Dashboard\Models\AreaAction;
use Route;
use DB;
use Validator;

class ResourceController extends Controller
{
    use ControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $action = $this->getCurrentAction();

        if (!$action) {
            return collect();
        }

        $table = $action->area->table;

        $currentModelActions = $this->_getCurrentActions(
            $request,
            $action->area->id
        );

        if ($action->area->model) {
            return response()->json(
                [
                    'data' => ($action->area->model)::all(),
                    'columns' => $action->area()->with(
                        ['columns.input.rules']
                    )->get(),
                    'actions' => $currentModelActions,
                ],
                200
            );
        }

        return response()->json(
            [
                'data' => DB::table($table)
                    ->all(),
                'columns' => $action->area()->with(
                    ['columns.input.rules']
                )->get(),
                'actions' => $currentModelActions,
            ],
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = $this->getCurrentAction();

        if (!$action) {
            return response()->json(['error' => 'Undefined action'], 404);
        }

        $validate = $this->_validate($action, $request);

        if ($validate !== true) {
            return response()->json($validate, 404);
        }

        if ($action->area->model) {
            return ($action->area->model)::create($request->all());
        }

        return response()->json(['error' => 'Model not saved'], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $action = $this->getCurrentAction();

        if (!$action) {
            return response()->json(['error' => 'Undefined action'], 404);
        }

        $validate = $this->_validate($action, $request);

        if ($validate !== true) {
            return response()->json($validate, 404);
        }

        if ($action->area->model) {
            $model = ($action->area->model)::find($id);
            foreach ($request->all() as $key => $value) {
                $model->$key = $value;
            }

            if ($model->save()) {
                return $model;
            }
        }

        return response()->json(['error' => 'Model not updated'], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $action = $this->getCurrentAction();

        if (!$action) {
            return response()->json(['error' => 'Undefined action'], 404);
        }

        if ($action->area->model) {
            $model = ($action->area->model)::find($id);
            if ($model) {
                return response()->json(
                    [
                        'status' => $model->delete(),
                        'id' => (int)$id,
                    ]
                );
            }
        }

        return response()->json(['error' => 'Model not deleted'], 404);
    }

    private function _getCurrentActions($request, $area_id)
    {
        $userActions = $request->user()->roles()->with(['actions'])->first();
        $currentModelActions = $userActions->actions->where(
            'area_id',
            $area_id
        );

        return $currentModelActions->pluck('name')->toArray();
    }

    private function _validate($action, $request)
    {
        $area = $action->area()->with(
            ['columns.input.rules']
        )->first();

        if (!$area) {
            return [
                'errors' => 'model not set!!',
            ];
        }

        $rules = $area->columns->map(
            function ($item) {
                $columnRules = $item->input->rules->map(
                    function ($rule) {
                        return $rule->rule;
                    }
                )->toArray();

                return [
                    $item->name => implode('|', $columnRules),
                ];

            }
        );

        $validator = Validator::make(
            $request->all(),
            $rules->collapse()->toArray()
        );

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
            ];
        }

        return true;
    }
}
