<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.07.17
 * Time: 18:40
 */
namespace MaxBoom\Dashboard\Http\Controllers;

use MaxBoom\Dashboard\Models\AreaAction;
use Route;

trait ControllerTrait {
    function getCurrentAction()
    {
        return AreaAction::whereAction(
            Route::getCurrentRoute()->uri()
        )->orWhere(
            'name',
            Route::currentRouteName()
        )->first();
    } // end _getCurrentAction
}