<?php

namespace MaxBoom\Dashboard;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\PassportServiceProvider;
use MaxBoom\Dashboard\Models\OauthClient;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class DashboardProvider extends ServiceProvider
{
    protected $policies = [
        \MaxBoom\Dashboard\Models\AreaAction::class => \MaxBoom\Dashboard\Policies\AreaActionPolicy::class,
    ];
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->_registerPassportServiceProvider();

        require_once __DIR__ . '/../database/seeds/UsersTableSeeder.php';
        require_once __DIR__ . '/../database/seeds/MenusTableSeeder.php';
        require_once __DIR__ . '/../database/seeds/InputRulesSeeder.php';
        require_once __DIR__ . '/../database/seeds/InputsSeeder.php';
        require_once __DIR__ . '/../database/seeds/AreaColumnsTableSeeder.php';

        $this->publishes([
            __DIR__ . '/../config/dashboard.php' => config_path('dashboard.php'),
        ], 'dashboard-config');

        $this->publishes([
            __DIR__ . '/../resources/assets' => resource_path('assets/vendor/maxboom/dashboard')
        ], 'dashboard-assets');

        $this->publishes([
            __DIR__ . '/../build' => public_path('vendor/build')
        ], 'dashboard-build');

        $this->_registerViews();

        $this->_loadRoutes();

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations/');

        $this->registerPolicies();

        $this->_addPassportRoutes();

        $this->_changeStandartConfigs();

        $this->_bindOauthClient();
    }

    private function _bindOauthClient()
    {
        return $this->app->singleton('DashboardOauthClient', function($app) {
                $clientID = config('dashboard.app_client_id');
                return OauthClient::whereId($clientID)->first();
            }
        );
    }

    private function _changeStandartConfigs()
    {
        config([
            'auth.providers.users.model' => config('dashboard.user_model_class'),
            'auth.guards.api.driver' => 'passport'
        ]);
    }

    private function _addPassportRoutes()
    {
        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(1));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }

    private function _registerPassportServiceProvider()
    {
        //\Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
        $provider = new PassportServiceProvider($this->app);
        $this->app->register($provider);
    }

    private function _registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'dashboard');

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('/vendor/maxboom/dashboard/views')
        ], 'dashboard-views');
    }

    private function _loadRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/dashboard.php', 'dashboard'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                \MaxBoom\Dashboard\Console\Commands\Install::class
            ]);
        }
    }
}
