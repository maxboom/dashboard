<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 17:34
 */

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole2Menu extends Model
{
    protected $fillable = [
        'menu_id',
        'role_id'
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'user_roles2menus';
        parent::__construct($attributes);
    }
}