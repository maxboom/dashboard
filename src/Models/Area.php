<?php

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
        'table',
        'caption',
        'model'
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'areas';
        parent::__construct($attributes);
    }

    public function actions()
    {
        return $this->hasMany(
            \MaxBoom\Dashboard\Models\AreaAction::class
        );
    }

    public function columns()
    {
        return $this->hasMany(
            \MaxBoom\Dashboard\Models\AreaColumn::class
        );
    }
}
