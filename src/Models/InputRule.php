<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 15:25
 */

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class InputRule extends Model
{
    protected $fillable = [
        'rule',
        'description'
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'input_rules';
        parent::__construct($attributes);
    }

    public function inputs()
    {
        return $this->belongsToMany(
            \MaxBoom\Dashboard\Models\Input::class,
            config('dashboard.table_prefix') . 'inputs',
            'input_id'
        );
    }
}