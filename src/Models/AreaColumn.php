<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 15:13
 */
namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class AreaColumn extends Model
{
    protected $fillable = [
        'name',
        'area_id',
        'input_id',
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'area_columns';
        parent::__construct($attributes);
    }

    public function area()
    {
        return $this->belongsTo(
            \MaxBoom\Dashboard\Models\Area::class,
            'area_id'
        );
    }

    public function input()
    {
        return $this->belongsTo(
            \MaxBoom\Dashboard\Models\Input::class,
            'input_id'
        );
    }
}
