<?php

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model
{
    public $table = 'oauth_access_tokens';
}
