<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 15:46
 */

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Input2Rule extends Model
{
    protected $fillable = [
        'input_id',
        'rule_id'
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'input2rules';
        parent::__construct($attributes);
    }
}