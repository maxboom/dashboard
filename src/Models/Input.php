<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.07.17
 * Time: 15:17
 */
namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model
{
    protected $fillable = [
        'name',
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'inputs';
        parent::__construct($attributes);
    }

    public function column()
    {
        return $this->belongsTo(
            \MaxBoom\Dashboard\Models\AreaColumn::class,
            'input_id'
        );
    }

    public function rules()
    {
        return $this->belongsToMany(
            \MaxBoom\Dashboard\Models\InputRule::class,
            config('dashboard.table_prefix') . 'input2rules',
            'input_id',
            'rule_id'
        )->withTimestamps();
    }
}