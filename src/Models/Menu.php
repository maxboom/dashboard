<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 17:26
 */

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'url',
        'parent_id',
        'caption'
    ];

    public function __construct(array $attributes = [])
    {
        $this->table = config('dashboard.table_prefix') . 'menus';
        parent::__construct($attributes);
    }

    public function roles()
    {
        return $this->belongsToMany(
            UserRole::class,
            (new  \MaxBoom\Dashboard\Models\UserRole2Menu())->getTable(),
            'menu_id',
            'role_id'
        )->withTimestamps();
    }

    // public static function all($collumns = [])
    // {
    //     $items = parent::all()->toArray();
    //     $tree = self::_createTree($items);
    //     //dd( $tree);
    //     return $tree;
    // }

    // public static function _createTree(&$array)
    // {
    //     $tree = array();

    //     // Create an associative array with each key being the ID of the item
    //     foreach($array as $k => &$v) $tree[$v['id']] = &$v;

    //     // Loop over the array and add each child to their parent
    //     foreach($tree as $k => &$v) {
    //         if(!$v['parent_id']) continue;
    //         $tree[$v['parent_id']]['children'][] = &$v;
    //     }

    //     // Loop over the array again and remove any items that don't have a parent of 0;
    //     foreach($tree as $k => &$v) {
    //         if(!$v['parent_id']) continue;
    //         unset($tree[$k]);
    //     }

    //     return $tree;
    // }
}