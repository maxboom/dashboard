<?php

namespace MaxBoom\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
    public $table = 'oauth_clients';

    protected $fillable = [
        'name',
        'redirect',
        'password_client',
        'url'
    ];
}
