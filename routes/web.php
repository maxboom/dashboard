<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 11:53
 */

Route::group([
    'middleware' => ['web']
], function() {
    Route::group([
        'prefix' => config('dashboard.url_prefix'),
    ], function() {
        Route::any('/{any?}', function() {
            // $viewPath = __DIR__.'/../resources/views';
            // View::addLocation($viewPath);
            // View::addNamespace('vue', $viewPath);
            // return View::make('dashboard::index');
            return view('dashboard::index');
        });
    });
});
