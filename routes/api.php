<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.17
 * Time: 11:53
 */

Route::group([
    'middleware' => ['web']
], function() {
    Route::group([
        'prefix' => config('dashboard.url_prefix').'/api',
        'middleware' => \MaxBoom\Dashboard\Http\Middleware\AreaAccess::class,
        'middleware' => \MaxBoom\Dashboard\Http\Middleware\RequestUrlAccess::class,
    ], function() {
        //AdminPanel
        Route::post('login', '\MaxBoom\Dashboard\Http\Controllers\AuthController@login');
        Route::post('refresh-token', '\MaxBoom\Dashboard\Http\Controllers\AuthController@refreshToken');

        Route::group(['middleware' => 'auth:api'], function (){

            if (\Schema::hasTable((new \MaxBoom\Dashboard\Models\Area())->getTable())) {
                foreach (\MaxBoom\Dashboard\Models\Area::whereNotNull('table')->get() as $area) {
                    Route::resource(
                        $area->table,
                        \MaxBoom\Dashboard\Http\Controllers\ResourceController::class
                    );
                }
            }
            //Custom routes
            Route::get('/users/create', [
                'as' => 'users.create',
                'uses' => \MaxBoom\Dashboard\Http\Controllers\UserController::class.'@create'
            ]);
            Route::post('/users', [
                'as' => 'users.store',
                'uses' => \MaxBoom\Dashboard\Http\Controllers\UserController::class.'@store'
            ]);
        });
    });
});
Route::group([
    'middleware' => ['web']
], function() {
    Route::group([
        'prefix' => '/api',
        'middleware' => \MaxBoom\Dashboard\Http\Middleware\AreaAccess::class,
        'middleware' => \MaxBoom\Dashboard\Http\Middleware\RequestUrlAccess::class,
    ], function() {
        //AdminPanel
        Route::post('login', '\MaxBoom\Dashboard\Http\Controllers\AuthController@login');
        Route::post('refresh-token', '\MaxBoom\Dashboard\Http\Controllers\AuthController@refreshToken');
    });
});